/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {  } from "react";
import {SafeAreaView,} from "react-native";
import AddScreen from './src/screens/AddScreen'

const App = () => {
 

  return (
    <SafeAreaView style={{  flex: 1,
      backgroundColor: "white",
      padding: 16,
      }}>
    <AddScreen />
       
    </SafeAreaView>
  );
};

export default App;

