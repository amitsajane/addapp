/* eslint-disable quotes */
/* eslint-disable no-trailing-spaces */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useEffect,useState } from "react";
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Alert
} from "react-native";

import admob, {
  MaxAdContentRating,
} from "@react-native-firebase/admob";

import {
  InterstitialAd,
  RewardedAd,
  RewardedAdEventType,
  TestIds,
  AdEventType,
  BannerAd,
  BannerAdSize,
} from "@react-native-firebase/admob";  

admob()
  .setRequestConfiguration({
    maxAdContentRating: MaxAdContentRating.PG,
    tagForChildDirectedTreatment: true,
    tagForUnderAgeOfConsent: true,
  })
  .then(() => {
  });

const interstialAdUnitId = __DEV__
  ? TestIds.INTERSTITIAL
  : "ca-app-pub-6379376707465782/9802919357";

const interstitial = InterstitialAd.createForAdRequest(
  interstialAdUnitId,
  { 
    requestNonPersonalizedAdsOnly: true,
    keywords: ["fashion", "clothing"],
  }
);

const rewardedAdUnitId = __DEV__
  ? TestIds.REWARDED
  : "ca-app-pub-6379376707465782/5264908546";

const rewarded = RewardedAd.createForAdRequest(
  rewardedAdUnitId,
  {
    requestNonPersonalizedAdsOnly: true,
    keywords: ["fashion", "clothing"],
  }
);

const bannerAdUnitId = __DEV__
  ? TestIds.BANNER
  : "ca-app-pub-6379376707465782/1666373067";

const AddScreen = () => {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    
    const interstitialAdEventListener = interstitial.onAdEvent(
      (type) => {
        if (type === AdEventType.LOADED) {
          setLoaded(true);
          console.log("Interstitial Ad Loaded");
        }
      }
    );

    interstitial.load();

    const rewardedAdEventListener = rewarded.onAdEvent(
      (type, error, reward) => {
        if (type === RewardedAdEventType.LOADED) {
          console.log("Rewarded Ad Loaded");
        }

        if (type === RewardedAdEventType.EARNED_REWARD) {
          console.log("User earned reward of ", reward);
        }
      }
    );

    rewarded.load();

    return () => {
      interstitialAdEventListener();
      rewardedAdEventListener();
    };
  }, []);
  if (!loaded) {
    return null;
  }

  const shwoAdd = () =>{
    Alert.alert('aan')
    rewarded.show()

  }

  return (
    <SafeAreaView style={styles.container}>
     
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <TouchableOpacity
          style={styles.buttonStyle}
          activeOpacity={0.5}
          onPress={() => interstitial.show()}
        >
          <Text style={styles.buttonTextStyle}>
            Show Interstitial Ad
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonStyle}
          activeOpacity={0.5}
          onPress={() =>shwoAdd()}
        >
          <Text style={styles.buttonTextStyle}>
            Show Rewarded Ad
          </Text>
        </TouchableOpacity>
      </View>

      <BannerAd
        unitId={bannerAdUnitId}
        size={BannerAdSize.ADAPTIVE_BANNER}
        requestOptions={{
          requestNonPersonalizedAdsOnly: true,
        }}
      />
    </SafeAreaView>
  );
};

export default AddScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    padding: 16,
  },
  heading: {
    fontSize: 20,
    textAlign: "center",
    marginTop: 30,
  },
  buttonStyle: {
    minWidth: 300,
    backgroundColor: "red",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#7DE24E",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 25,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
});